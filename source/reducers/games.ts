import {
  FETCH_GAMES_FAILED,
  FETCH_GAMES_STARTED,
  FETCH_GAMES_SUCCEEDED,
  FetchGamesFailed,
  FetchGamesStarted,
  FetchGamesSucceeded,
} from 'ventura/actions/games';
import { GamesState } from 'ventura/state/games';

type Actions = FetchGamesStarted | FetchGamesSucceeded | FetchGamesFailed;

const initialState: GamesState = {
  games: [],
  loading: false,
  error: null,
};

export function gamesReducer(
  state: GamesState = initialState,
  action: Actions,
): GamesState {
  switch (action.type) {
    case FETCH_GAMES_STARTED:
      return { ...state, loading: true, error: null };
    case FETCH_GAMES_FAILED:
      return {
        ...state,
        loading: false,
        error: action.error || 'Unknown Error',
      };
    case FETCH_GAMES_SUCCEEDED:
      return { ...state, loading: false, error: null, games: action.games };
  }

  return state;
}
