export interface Game {
  name: string;
  slug: string;
  icon: number;
  order: number;
  supportsAddons: boolean;
  supportsVoice: boolean;
  fileNames: string[];
  sectionNames: string[];
}
