import { Game } from 'ventura/models/game';

export interface GamesState {
  games: Game[];
  loading: boolean;
  error: null | string;
}
