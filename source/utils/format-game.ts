import { Game } from 'ventura/models/game';

function safeStringExtractionReducer(key: string) {
  return (files: string[], item: any) =>
    item[key] ? [...files, item[key]] : files;
}

export function formatGameReducer(allGames: Game[], rawGame: any): Game[] {
  const name: string = rawGame.Name;
  const slug: string = rawGame.Slug;
  const icon: number = Number(rawGame.ID) || 0;
  const rawOrder = rawGame.Order;
  const order: number = isNaN(rawOrder) // If NaN, then just throw it at then
    ? Number.MAX_SAFE_INTEGER // NaN
    : Number(rawOrder); // is a number
  const supportsAddons: boolean = rawGame.SupportsAddons || false;
  const supportsVoice: boolean = rawGame.SupportsVoice || false;
  const fileNames: string[] = (rawGame.GameFiles || []).reduce(
    safeStringExtractionReducer('FileName'),
    [],
  );
  const sectionNames = (rawGame.CategorySections || []).reduce(
    safeStringExtractionReducer('Name'),
    [],
  );
  if (!name || !slug) {
    //If no game or slug, then it's not going to work out
    return allGames;
  }
  const newGame: Game = {
    name,
    slug,
    icon,
    order,
    supportsAddons,
    supportsVoice,
    fileNames,
    sectionNames,
  };
  return [...allGames, newGame];
}
