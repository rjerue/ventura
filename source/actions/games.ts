import { Dispatch } from 'redux';
import { config } from 'ventura/globals';
import { Game } from 'ventura/models/game';
import { formatGameReducer } from 'ventura/utils/format-game';
// Fetch Games Started
export const FETCH_GAMES_STARTED = 'FETCH_GAMES_STARTED';
export type FetchGamesStarted = {
  type: typeof FETCH_GAMES_STARTED;
};

function fetchGamesStarted(): FetchGamesStarted {
  return { type: FETCH_GAMES_STARTED };
}

// Fetch Games Succeeded
export const FETCH_GAMES_SUCCEEDED = 'FETCH_GAMES_SUCCEEDED';
export type FetchGamesSucceeded = {
  type: typeof FETCH_GAMES_SUCCEEDED;
  games: Game[];
};

function fetchGamesSucceeded(games: Game[]): FetchGamesSucceeded {
  return { type: FETCH_GAMES_SUCCEEDED, games };
}

// Fetch Games Failed
export const FETCH_GAMES_FAILED = 'FETCH_GAMES_FAILED';
export type FetchGamesFailed = {
  type: typeof FETCH_GAMES_FAILED;
  error?: string;
};

function fetchGamesFailed(error?: string): FetchGamesFailed {
  return { type: FETCH_GAMES_FAILED, error };
}

// Fetch Games Thunk
export function fetchGames() {
  return async (dispatch: Dispatch<any>) => {
    dispatch(fetchGamesStarted());
    try {
      const response = await fetch(config.gamesDataURL);
      const { data } = await response.json();
      const games: Game[] = data.reduce(formatGameReducer, []);
      const gamesSorted = games.sort(
        (
          { order: leftOrder, name: leftName },
          { order: rightOrder, name: rightName },
        ): number => {
          const diff = leftOrder - rightOrder;
          if (diff !== 0) {
            return diff;
            //if two items are of the same order, we will sort by name alphabetically
          } else if (leftName < rightName) {
            return -1;
          } else if (leftName > rightName) {
            return 1;
          } else {
            return 0;
          }
        },
      );
      console.log(gamesSorted);
      dispatch(fetchGamesSucceeded(gamesSorted));
      return gamesSorted;
    } catch (error) {
      const errorMessage = error.message;
      console.error(errorMessage);
      dispatch(fetchGamesFailed(errorMessage));
      return { error };
    }
  };
}
