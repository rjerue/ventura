import React from 'react';
import { MemoryRouter } from 'react-router';

export function RouterWrapper({ children }: { children: React.ReactNode }) {
  return <MemoryRouter>{children}</MemoryRouter>;
}
