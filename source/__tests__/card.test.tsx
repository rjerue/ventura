import { cleanup, render } from '@testing-library/react';
import React from 'react';
import { GameCard } from 'ventura/components/GameCard';
import { initGlobals } from 'ventura/globals';
import { Game } from 'ventura/models/game';
import { RouterWrapper } from './utils/wrapper';

const game: Game = {
  name: "Ryan's world",
  slug: 'ryan',
  icon: 1,
  order: 1,
  supportsVoice: true,
  supportsAddons: false,
  fileNames: ['Hello', 'World'],
  sectionNames: ['Head', 'torso', 'feet'],
};

beforeEach(() => {
  initGlobals();
});

afterEach(cleanup);

it('Expect Game Card to Render', () => {
  const card = render(
    <RouterWrapper>
      <GameCard game={game} />
    </RouterWrapper>,
  );
  const name = card.findByText(game.name);
  expect(name).toBeTruthy();
});
