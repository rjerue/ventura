import {
  Avatar,
  Card,
  CardContent,
  CardHeader,
  Typography,
} from '@material-ui/core';
import React, { memo } from 'react';
import { Link } from 'react-router-dom';
import { config } from 'ventura/globals';
import { Game } from 'ventura/models/game';
import './GameCard-style.scss';

interface GameCardProps {
  game: Game;
  children?: React.ReactNode;
}
// tslint:disable-next-line:variable-name
export const GameCard = memo(function GameCardComponent({
  game,
  children,
}: GameCardProps) {
  return (
    <Card className="card" key={game.name}>
      <CardHeader
        avatar={
          <Avatar
            src={config.gameIconURLTemplate(game.icon)}
            alt={`${game.name} icon`}
          />
        }
        title={<Link to={`/games/${game.slug}`}>{game.name}</Link>}
      />
      <CardContent>
        {game.supportsVoice ? (
          <Typography className="supported">✓ Supports voice</Typography>
        ) : (
          <Typography className="not-supported">
            X Does not support voice
          </Typography>
        )}
        {game.supportsAddons ? (
          <Typography className="supported">✓ Supports addons</Typography>
        ) : (
          <Typography className="not-supported">
            X Does not support addons
          </Typography>
        )}
        {children}
      </CardContent>
    </Card>
  );
});
