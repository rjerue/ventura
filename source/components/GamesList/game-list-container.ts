import { connect } from 'react-redux';
import { fetchGames } from 'ventura/actions/games';
import { GlobalState } from 'ventura/state/global';
import { GameListPage, ReduxDispatchProps, ReduxStateProps } from './game-list';

function mapStateToProps({
  games: { games, loading, error },
}: GlobalState): ReduxStateProps {
  return {
    games,
    loading,
    error,
  };
}

const mapDispatchToProps: ReduxDispatchProps = {
  fetchGames,
};

// tslint:disable-next-line:variable-name
export const GameListContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(GameListPage);
