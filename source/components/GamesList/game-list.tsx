import { Button, CircularProgress, TextField } from '@material-ui/core';
import fuzzysearch from 'fuzzy-search';
import React, { useState } from 'react';
import { Game } from 'ventura/models/game';
import { GamesState } from 'ventura/state/games';
import { GameCard } from '../GameCard';
import './game-list-style.scss';

export type ReduxStateProps = {} & GamesState;

export interface ReduxDispatchProps {
  fetchGames: any;
}

type Props = ReduxStateProps & ReduxDispatchProps;

interface GameListProps {
  games: Game[];
  filter: string;
}

function GameList({ games, filter }: GameListProps) {
  return (
    <>
      {games.reduce((allGames, game) => {
        const searcher = new fuzzysearch([game.name]);
        if (!filter || searcher.search(filter).length > 0) {
          return [...allGames, <GameCard key={game.name} game={game} />];
        }
        return allGames;
      }, [])}
    </>
  );
}

export function GameListPage({ games, loading, error, fetchGames }: Props) {
  const [filter, setFilter] = useState('');
  if (loading) {
    return <CircularProgress disableShrink />;
  }
  return (
    <>
      <Button onClick={fetchGames}>Refresh Games</Button>
      {error ? (
        <div>{error}</div>
      ) : (
        <>
          <form className="search-input" noValidate autoComplete="off">
            <TextField
              label="Search by game name"
              value={filter}
              onChange={e => setFilter(e.target.value)}
            />
          </form>
          <GameList games={games} filter={filter} />
        </>
      )}
    </>
  );
}
