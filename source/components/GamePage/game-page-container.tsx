import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { createSelector } from 'reselect';
import { GlobalState } from 'ventura/state/global';
import { GamePage } from '.';

const findGame = (state: GlobalState, desiredSlug: string) => {
  return state.games.games.find(({ slug }) => {
    return slug === desiredSlug;
  });
};

const findGameSelector = createSelector(
  findGame,
  game => game,
);

interface PageParams {
  game: string;
}

function mapStateToProps(state: GlobalState, props: RouteComponentProps) {
  const { game: gameName } = props.match.params as PageParams;
  const game = findGameSelector(state, gameName);
  return {
    game,
  };
}

// tslint:disable-next-line:variable-name
export const GamePageContainer = connect(mapStateToProps)(GamePage);
