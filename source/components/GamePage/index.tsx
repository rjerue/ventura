import Typography from '@material-ui/core/Typography';
import React from 'react';
import { Game } from 'ventura/models/game';
import { GameCard } from '../GameCard';

interface GamePageProps {
  game?: Game;
}

export function GamePage({ game }: GamePageProps) {
  if (!game) {
    return <div>Game not found!</div>;
  }
  const fileNames = game.fileNames.join(', ');
  const sectionNames = game.sectionNames.join(', ');
  return (
    <GameCard game={game}>
      <Typography>Slug: {game.slug}</Typography>
      {fileNames && <Typography>Game File Names: {fileNames}</Typography>}
      {sectionNames && (
        <Typography>Game Category Names: {sectionNames}</Typography>
      )}
    </GameCard>
  );
}
