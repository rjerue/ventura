import { Container } from '@material-ui/core';
import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { ReduxDispatchProps } from '../GamesList/game-list';
import './layout-style.scss';
import logo from './logo.svg';

type LayoutProps = { children: React.ReactNode } & ReduxDispatchProps;

export function Layout({ children, fetchGames }: LayoutProps) {
  useEffect(() => {
    fetchGames();
  });
  return (
    <div className="game-list__root">
      <Link to="/">
        <img className="logo" src={logo} />
      </Link>
      <h1>Twitch React Test</h1>
      <Container className="game-list" maxWidth="sm">
        {children}
      </Container>
    </div>
  );
}
