import { connect } from 'react-redux';
import { fetchGames } from 'ventura/actions/games';
import { ReduxDispatchProps } from '../GamesList/game-list';
import { Layout as LayoutComponent } from './Layout';

const mapDispatchToProps: ReduxDispatchProps = {
  fetchGames,
};

// tslint:disable-next-line:variable-name
export const Layout = connect(
  null,
  mapDispatchToProps,
)(LayoutComponent);
