import { createBrowserHistory } from 'history';
import * as React from 'react';
import { Provider } from 'react-redux';
import { Route, Router, Switch } from 'react-router';
import { store } from 'ventura/globals';
import { GamePageContainer } from './GamePage/game-page-container';
import { GameListContainer } from './GamesList/game-list-container';
import { Layout } from './Layout/layout-container';
import './root.scss';
export class Root extends React.Component<{}> {
  render() {
    return (
      <Provider store={store}>
        <Router history={createBrowserHistory()}>
          <Layout>
            <Switch>
              <Route path="/" exact component={GameListContainer} />
              <Route path="/games/:game" component={GamePageContainer} />
            </Switch>
          </Layout>
        </Router>
      </Provider>
    );
  }
}
